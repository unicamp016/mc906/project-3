## Visual Line Following using Fuzzy
This project aims to build a control system for a drone tasked with following a line using fuzzy logic to determine its steering angle. We apply a constant forward velocity to the drone to ensure its always progressing use a fuzzy system with information gathered from the camera to output the desired steering angle.

### Input Variables
* ``line_start``: A float normalized within the range $`[0, 1]`$ indicating the approximate position where the line enters the image through the bottom border. If the line is centered at the bottom border's rightmost pixel, this inputs has value $`0`$. If it is centered at the leftmost pixel, it has value $`1`$. The values increase linearly with the pixel distance.
* ``line_end``: A float normalized within the range $`[0, 1]`$ that indicates where the line exits the image. The line can exit the image from any of the three borders remaining borders: left, top, and right. A value of $`0`$ indicates that the line exits from the bottom pixel on the left border and a value of $`1`$ signals that it exits through the bottom pixel of the right border. A value of $`0.5`$ means the line exits trough the center of the top border.

### Output Variable
* ``steering_angle``: Angle that the drone should be steered in degrees. Values range between $`[-90, 90]`$.

### Fuzzy Rules
1. If the line starts at the left, then steer a little bit to the left
2. If the line starts at the right, then steer a little bit to the right
3. If the line exits through the left, then steer a lot to the left
4. If the line exits through the right, then steer a lot to the right
5. If the line starts at the middle OR the line exits through the top, then don't steer
