import os

import cv2
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

import utils

MIN_SPEED = 0.005
MAX_SPEED = 0.015
MID_SPEED = (MIN_SPEED + MAX_SPEED)/2

membership_fn = {
    'triangular': {
        'fn': fuzz.trimf,
        'input_low': {'abc': [0, 0, 0.5]},
        'input_med': {'abc': [0.0, 0.5, 1.0]},
        'input_high': {'abc': [0.5, 1.0, 1.0]},
        'steer_low': {'abc': [-1.0, -1.0, -0.5]},
        'steer_med': {'abc': [-0.5, 0.0, 0.5]},
        'steer_high': {'abc': [0.5, 1.0, 1.0]},
        'vel_low': {'abc': [0, 0, 0.5]},
        'vel_med': {'abc': [0.0, 0.5, 1.0]},
        'vel_high': {'abc': [0.5, 1.0, 1.0]},
    },
    'gaussian': {
        'fn': fuzz.gaussmf,
        'input_low': {'mean': 0, 'sigma':0.2},
        'input_med': {'mean': 0.5, 'sigma':0.2},
        'input_high': {'mean': 1.0, 'sigma':0.2},
        'steer_low': {'mean': -1.0, 'sigma':0.25},
        'steer_med': {'mean': 0, 'sigma':0.25},
        'steer_high': {'mean': 1.0, 'sigma':0.25},
        'vel_low': {'mean': 0, 'sigma': 0.2},
        'vel_med': {'mean': 0.5, 'sigma': 0.2},
        'vel_high': {'mean': 1.0, 'sigma': 0.2}
    },
    'trapezoidal': {
        'fn': fuzz.trapmf,
        'input_low': {'abcd': [0, 0.2, 0.3, 0.5]},
        'input_med': {'abcd': [0.2, 0.4, 0.5, 0.8]},
        'input_high': {'abcd': [0.5, 0.7, 0.8, 1.0]},
        'steer_low': {'abcd': [-1.0, -0.8, -0.7, -0.4]},
        'steer_med': {'abcd': [-0.6, -0.2, 0.2, 0.6]},
        'steer_high': {'abcd': [0.4, 0.7, 0.8, 1.0]},
        'vel_low': {'abcd': [0, 0.2, 0.3, 0.5]},
        'vel_med': {'abcd': [0.2, 0.4, 0.5, 0.8]},
        'vel_high': {'abcd': [0.5, 0.7, 0.8, 1.0]},
    }
}

class FuzzySystem:

    def __init__(self, config, output_dir):
        self.output_dir = output_dir
        self.mf = config['membership_fn']
        self.defuzz_mode = config['defuzz_fn']
        self.inference = config['inference']
        self.x_line_end = np.linspace(0, 1, 41)
        self.x_cdist = np.linspace(0, 1, 41)
        self.x_steer_angle = np.linspace(-1, 1, 41)
        self.x_velocity = np.linspace(0, 1, 41)
        self.save_path = lambda path: os.path.join(
            output_dir,
            f'{self.mf}-{self.defuzz_mode}-{self.inference}-{path}.pdf'
        )

        if self.inference == 'mamdani':
            self.infer_fn = np.fmin
        elif self.inference == 'larsen':
            self.infer_fn = np.dot
        else:
            raise ValueError("Invalid inference function argument")

        # Get param dict for membership function
        params = membership_fn[self.mf]

        # Fuzzy membership functions for line_end
        self.line_end_left = params['fn'](self.x_line_end, **params['input_low'])
        self.line_end_top = params['fn'](self.x_line_end, **params['input_med'])
        self.line_end_right = params['fn'](self.x_line_end, **params['input_high'])

        # Fuzzy membership functions for cdist
        self.cdist_low = params['fn'](self.x_cdist, **params['input_low'])
        self.cdist_med = params['fn'](self.x_cdist, **params['input_med'])
        self.cdist_high = params['fn'](self.x_cdist, **params['input_high'])

        # Fuzzy membership functions for steering
        self.steer_left  = params['fn'](self.x_steer_angle, **params['steer_low'])
        self.steer_low = params['fn'](self.x_steer_angle, **params['steer_med'])
        self.steer_right = params['fn'](self.x_steer_angle, **params['steer_high'])

        # Fuzzy membership functions for velocity
        self.velocity_low  = params['fn'](self.x_velocity, **params['vel_low'])
        self.velocity_med = params['fn'](self.x_velocity, **params['vel_med'])
        self.velocity_high = params['fn'](self.x_velocity, **params['vel_high'])


    def plot_fuzzy_system(self, line_end=None, cdist=None):
        fig, ax = plt.subplots(nrows=1, figsize=(8, 4))
        ax.plot(self.x_line_end, self.line_end_left, 'b', linewidth=1.5, label='Left')
        ax.plot(self.x_line_end, self.line_end_top, 'g', linewidth=1.5, label='Top')
        ax.plot(self.x_line_end, self.line_end_right, 'r', linewidth=1.5, label='Right')
        ax.legend(loc='upper right')
        fig.savefig(self.save_path('line-end'))
        plt.close(fig)

        fig, ax = plt.subplots(nrows=1, figsize=(8, 4))
        ax.plot(self.x_cdist, self.cdist_low, 'b', linewidth=1.5, label='Far')
        ax.plot(self.x_cdist, self.cdist_med, 'g', linewidth=1.5, label='Medium')
        ax.plot(self.x_cdist, self.cdist_high, 'r', linewidth=1.5, label='Close')
        ax.legend(loc='upper right')
        fig.savefig(self.save_path('cdist'))
        plt.close(fig)

        fig, ax = plt.subplots(nrows=1, figsize=(8, 4))
        ax.plot(self.x_steer_angle, self.steer_left, 'b', linewidth=1.5, label='Left')
        ax.plot(self.x_steer_angle, self.steer_low, 'g', linewidth=1.5, label='Center')
        ax.plot(self.x_steer_angle, self.steer_right, 'r', linewidth=1.5, label='Right')
        ax.legend(loc='upper right')
        fig.savefig(self.save_path('steer-angle'))
        plt.close(fig)

        fig, ax = plt.subplots(nrows=1, figsize=(8, 4))
        ax.plot(self.x_velocity, self.velocity_low, 'b', linewidth=1.5, label='Slow')
        ax.plot(self.x_velocity, self.velocity_med, 'g', linewidth=1.5, label='Moderate')
        ax.plot(self.x_velocity, self.velocity_high, 'r', linewidth=1.5, label='Fast')
        ax.legend(loc='upper right')
        fig.savefig(self.save_path('velocity'))
        plt.close(fig)

        # Save an example of inference for each input
        if line_end is not None:
            self.compute_steer_angle(line_end, show=True)

        if cdist is not None:
            self.compute_velocity(cdist, show=True)

    def recursive_fmax(self, activations):
        if len(activations) < 2:
            raise InvalidArgumentException
        elif len(activations) == 2:
            return np.fmax(activations[0], activations[1])
        else:
            return np.fmax(activations.pop(), self.recursive_fmax(activations))

    def compute_velocity(self, cdist, show=False):

        if cdist < 0.001:
            return MAX_SPEED

        cdist_interp_membership = \
            lambda mf: fuzz.interp_membership(
                x=self.x_cdist,
                xmf=mf,
                xx=cdist
            )
        cdist_level_low = cdist_interp_membership(self.cdist_low)
        cdist_level_med = cdist_interp_membership(self.cdist_med)
        cdist_level_high = cdist_interp_membership(self.cdist_high)

        velocity_activation_low = self.infer_fn(cdist_level_low, self.velocity_high)
        velocity_activation_med = self.infer_fn(cdist_level_med, self.velocity_med)
        velocity_activation_high = self.infer_fn(cdist_level_high, self.velocity_low)

        aggregated = self.recursive_fmax([
            velocity_activation_low,
            velocity_activation_med,
            velocity_activation_high
        ])

        velocity = fuzz.defuzz(self.x_velocity, aggregated, self.defuzz_mode)
        velocity_activation = fuzz.interp_membership(self.x_velocity, aggregated, velocity)
        velocity_final = velocity*(MAX_SPEED - MIN_SPEED) + MIN_SPEED

        if show:
            velocity_zeros = np.zeros_like(self.x_velocity)

            # Draw output membership activity for each rule
            fig, ax0 = plt.subplots(figsize=(8, 4))
            ax0.fill_between(self.x_velocity, velocity_zeros, velocity_activation_low, facecolor='b', alpha=0.7)
            ax0.plot(self.x_velocity, self.velocity_low, 'b', linewidth=0.5, linestyle='--')
            ax0.fill_between(self.x_velocity, velocity_zeros, velocity_activation_med, facecolor='g', alpha=0.7)
            ax0.plot(self.x_velocity, self.velocity_med, 'g', linewidth=0.5, linestyle='--')
            ax0.fill_between(self.x_velocity, velocity_zeros, velocity_activation_high, facecolor='r', alpha=0.7)
            ax0.plot(self.x_velocity, self.velocity_high, 'r', linewidth=0.5, linestyle='--')

            fig.savefig(self.save_path('velocity-activations'))
            plt.close(fig=fig)

            # Draw aggregated membership and result
            fig, ax0 = plt.subplots(figsize=(8, 4))
            ax0.plot(self.x_velocity, self.velocity_low, 'b', linewidth=0.5, linestyle='--')
            ax0.plot(self.x_velocity, self.velocity_med, 'g', linewidth=0.5, linestyle='--')
            ax0.plot(self.x_velocity, self.velocity_high, 'r', linewidth=0.5, linestyle='--')
            ax0.fill_between(self.x_velocity, velocity_zeros, aggregated, facecolor='Orange', alpha=0.7)
            ax0.plot([velocity, velocity], [0, velocity_activation], 'k', linewidth=1.5, alpha=0.9)
            fig.savefig(self.save_path('velocity-aggregated'))


            # img = utils.fig_to_img(fig)
            # cv2.imshow('Fuzzy Velocity', img)
            # cv2.waitKey(1)

        return velocity_final

    def compute_steer_angle(self, line_end, show=False):

        line_end_interp_membership = \
            lambda mf: fuzz.interp_membership(
                x=self.x_line_end,
                xmf=mf,
                xx=line_end
            )
        line_end_level_right = line_end_interp_membership(self.line_end_right)
        line_end_level_left = line_end_interp_membership(self.line_end_left)
        line_end_level_top = line_end_interp_membership(self.line_end_top)

        steer_activation_left = self.infer_fn(line_end_level_left, self.steer_left)
        steer_activation_right = self.infer_fn(line_end_level_right, self.steer_right)
        steer_activation_low = self.infer_fn(line_end_level_top, self.steer_low)

        aggregated = self.recursive_fmax([
            steer_activation_left,
            steer_activation_low,
            steer_activation_right
        ])

        steer_angle = fuzz.defuzz(self.x_steer_angle, aggregated, self.defuzz_mode)
        steer_angle_activ = fuzz.interp_membership(self.x_steer_angle, aggregated, steer_angle)

        if show:
            steer_zeros = np.zeros_like(self.x_steer_angle)

            # Plot activations for each rule
            fig, ax0 = plt.subplots(figsize=(8, 4))
            ax0.fill_between(self.x_steer_angle, steer_zeros, steer_activation_left, facecolor='b', alpha=0.7)
            ax0.plot(self.x_steer_angle, self.steer_left, 'b', linewidth=0.5, linestyle='--')
            ax0.fill_between(self.x_steer_angle, steer_zeros, steer_activation_low, facecolor='r', alpha=0.7)
            ax0.plot(self.x_steer_angle, self.steer_low, 'g', linewidth=0.5, linestyle='--')
            ax0.fill_between(self.x_steer_angle, steer_zeros, steer_activation_right, facecolor='k', alpha=0.7)
            ax0.plot(self.x_steer_angle, self.steer_right, 'r', linewidth=0.5, linestyle='--')
            fig.savefig(self.save_path('steer-angle-activations'))
            plt.close(fig=fig)

            # Plot aggregated activations and result
            fig, ax0 = plt.subplots(figsize=(8, 4))
            ax0.plot(self.x_steer_angle, self.steer_left, 'b', linewidth=0.5, linestyle='--')
            ax0.plot(self.x_steer_angle, self.steer_low, 'g', linewidth=0.5, linestyle='--')
            ax0.plot(self.x_steer_angle, self.steer_right, 'r', linewidth=0.5, linestyle='--')
            ax0.fill_between(self.x_steer_angle, steer_zeros, aggregated, facecolor='Orange', alpha=0.7)
            ax0.plot([steer_angle, steer_angle], [0, steer_angle_activ], 'k', linewidth=1.5, alpha=0.9)
            fig.savefig(self.save_path('steer-angle-aggregated'))

            # img = utils.fig_to_img(fig)
            # cv2.imshow('Fuzzy Activations', img)
            # cv2.waitKey(1)

            plt.close(fig=fig)


        return steer_angle
