import os
import glob
import argparse

import cv2
import numpy as np

import robot
import utils
import system

def main(config, drone, output_dir, log_dir, model, show=False):

    fuzzy_system = system.FuzzySystem(config, output_dir)
    fuzzy_system.plot_fuzzy_system(line_end=0.7, cdist=0.7)

    while True:
        image = drone.read_vision_sensor()

        # Check if we are already at the finish line
        green_mask = utils.get_mask(image, color='g')
        green_ratio = np.sum(green_mask > 0)/(green_mask.shape[0]*green_mask.shape[1])

        if show:
            cv2.imshow("Green", green_mask)

        if green_ratio > 0.10:
            utils.cprint("Reached finish line!", "g")
            return

        # Get a binary mask where the yellow is white foreground
        yellow_mask = utils.get_mask(image, color='y')

        # Get fuzzy inputs
        line_end = utils.get_line_end(yellow_mask)
        cdist = utils.get_centroid_dist(yellow_mask)

        # Compute fuzzy steering angle and velocity
        steering_angle = fuzzy_system.compute_steer_angle(line_end, show=show)
        if model == 'a' or model == 'test':
            velocity = 0.01
        else:
            velocity = fuzzy_system.compute_velocity(cdist, show=show)

        # Move and steer drone according to fuzzy output
        drone.move_forward(velocity)
        drone.steer_angle(steering_angle)

        # Update map grid with current drone position
        drone.update_drone_path()

        if show:
            # Show trajectory
            cv2.imshow("Trajectory", drone.grid)

            # Show masked line
            cv2.imshow('Vision Sensor', yellow_mask)

            cv2.waitKey(1)

def shutdown(config, drone, start_time, log_dir, build_grid):
    # Stop simulation
    drone.stop_sim()

    # Get simulation duration
    elapsed_time = drone.time() - start_time

    drone.save(image="path")

    dist_cost = None
    if args.build_grid:
        drone.save(image="ground_truth")
        drone.save(image="grid")
        msd, rmsd, hd = utils.get_metrics(drone.ground_truth, drone.path)
        dist_cost = msd + 2*rmsd + hd
        with open(os.path.join(log_dir, "metrics.txt"), "a") as f:
            f.write(f"Config: {config}\n")
            f.write(f"MSD {msd:.3f}\n")
            f.write(f"RMSD {rmsd:.3f}\n")
            f.write(f"HD {hd:.3f}\n")
            f.write(f"D {dist_cost:.3f}\n")
            f.write(f"Time {elapsed_time/1000:.3f} ms\n")
            f.write("===============================\n")
    return dist_cost

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output_dir", type=str, default='bin')
    parser.add_argument("-l", "--log_dir", type=str, default='logs')
    parser.add_argument("--show", action='store_true')
    parser.add_argument("--build_grid", action='store_true')
    parser.add_argument("--model", default="b", help="Fuzzy system model to use")
    args = parser.parse_args()

    # Create model output dir in case it doesn't exist, otherwise clean it up
    output_dir = os.path.join(args.output_dir, f"model-{args.model}")
    log_dir = os.path.join(args.log_dir, f"model-{args.model}")

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    else:
        for file in glob.glob(f"{output_dir}/*"):
            os.remove(file)

    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)
    else:
        for file in glob.glob(f"{log_dir}/*"):
            os.remove(file)

    params = {
        'membership_fn': ['trapezoidal', 'gaussian', 'triangular'],
        'defuzz_fn': ['bisector', 'centroid'],
        'inference': ['mamdani', 'larsen']
    }

    # Create an initial base config
    best_dist = float('inf')
    for k, v in params.items():
        best_config[k] = v[0]
    first_param = True

    for p in params:
        for v in params[p]:

            # In case we already ran tests for this param
            if not first_param and best_config[p] == v:
                continue

            cur_config = best_config.copy()
            cur_config[p] = v

            print(cur_config)

            # Initalize drone object from scene
            drone = robot.Drone(cur_config, output_dir, args.build_grid)

            # Keep track of simulation start time
            start_time = drone.time()

            main(cur_config, drone, output_dir, log_dir, args.model, args.show)
            dist = shutdown(cur_config, drone, start_time, log_dir, args.build_grid)

            # Update best config
            if dist < best_dist:
                best_dist = dist
                best_config = cur_config.copy()

        first_param = False

    cv2.destroyAllWindows()
