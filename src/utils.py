import sys
import math

import cv2
import numpy as np
from scipy.ndimage import morphology
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

# Flags
BORDER_DOWN = 1
BORDER_UP = 2
BORDER_RIGHT = 4
BORDER_LEFT = 8

def cprint(text, color, **kwargs):
    style = '\033[38;5;'
    reset = '\033[0m'
    if color == 'g':
        code = '34'
    elif color == 'r':
        code = '1'
    elif color == 'b':
        code = '3'
    print(f"{style}{code}m{text}{reset}", **kwargs)

def eprint(*args, **kwargs):
    """
        Wrapper to print to stderr.
    """
    print(*args, file=sys.stderr, **kwargs)

def vrep_to_array(image, resolution):
    """
        Converts vrep image to a numpy BGR array.

    Args:
        image: List with raw pixel data received from vrep image sensor.
        resolution: Tuple with desired image resolution to create numpy array.

    Returns:
        img: Image as a numpy array in BGR.

    """
    img = np.array(image).astype(np.uint8)
    img = np.reshape(img, (resolution[1], resolution[0], -1))
    img = cv2.flip(img, 0)  # Flip image over x-axis
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    return img

def get_mask(image, color='y'):
    """Retrieve a binary mask where whites (255) represent the foreground and
    black (0) the background. We consider the foreground only yellow objects
    with pixel values in the range [20, 200, 201] to [40, 270, 281] in the HSV
    color space.

    Args:
        image: Input image

    Returns:
        mask: Binary output image of the same size of input with white pixels where
        the interval specified by `color` is respected.

    """
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    if color == 'y':
        upper = np.array([40, 270, 281])
        lower = np.array([20, 200, 201])
    elif color == 'g':
        upper = np.array([71, 180, 270])
        lower = np.array([51, 110, 200])
    mask = cv2.inRange(hsv, lower, upper)
    return mask

def border_indices(rows, cols, flags=BORDER_DOWN):
    """Generator that yields the border indices given to traverse a matrix given
    the number of rows and columns.
    Args:
        rows: Number of rows in the matrix
        cols: Number of columns in the matrix
        flags: Bitmask specifying the borders to traverse from the matrix

    Returns:
        Generator with the indices of the specified borders traversing the matrix.
    """
    if flags & BORDER_DOWN:
        r = rows - 1
        for c in reversed(range(cols)):
            yield r, c
    if flags & BORDER_LEFT:
        c = 0
        for r in reversed(range(rows)):
            yield r, c
    if flags & BORDER_UP:
        r = 0
        for c in range(cols):
            yield r, c
    if flags & BORDER_RIGHT:
        c = cols - 1
        for r in range(rows):
            yield r, c

def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return int(qx), int(qy)

def get_metrics(path1, path2):
    """
    Compute surface distance metrics between two paths.
    Args:
        path1: 2-D or 3-D numpy array of any type where white pixels (255) will
               be considered as belonging to the background.
        path2: Same as path1, the metrics are symmetric
    Returns:
        msd: Mean surface distance between two paths
        rmsd: Root mean square distance between two paths
        hd: Hausdorff distance between two paths
    Reference: https://mlnotebook.github.io/post/surface-distance-function/
    """
    if path1.ndim == 3:
        path1 = cv2.cvtColor(path1, cv2.COLOR_BGR2GRAY)
    if path2.ndim == 3:
        path2 = cv2.cvtColor(path2, cv2.COLOR_BGR2GRAY)

    # Invert images so that white is foreground and cast to boolean
    path1 = (~path1).astype(np.bool)
    path2 = (~path2).astype(np.bool)

    kernel = np.ones((3,3), dtype=np.bool)

    S = path1 ^ morphology.binary_erosion(path1, kernel)
    Sprime = path2 ^ morphology.binary_erosion(path2, kernel)

    # Apply euclidean transform to matrix
    dta = morphology.distance_transform_edt(~S, sampling=1)
    dtb = morphology.distance_transform_edt(~Sprime, sampling=1)

    # Compute surface distances
    sds = np.concatenate([np.ravel(dta[Sprime != 0]), np.ravel(dtb[S != 0])])

    msd = sds.mean()
    rms = np.sqrt((sds**2).mean())
    hd  = sds.max()

    return msd, rms, hd

def get_centroid_dist(mask):
    h, w = mask.shape

    # Find all contours
    _, contours, _ = cv2.findContours(mask, mode=cv2.RETR_LIST, method=cv2.CHAIN_APPROX_SIMPLE)

    # Sort contours by area
    contours = [(cnt, cv2.contourArea(cnt)) for cnt in contours]
    contours = sorted(contours, key=lambda cnt: cnt[1])

    # Compute centroid for largest contour
    M = cv2.moments(contours[-1][0])
    cx = int(M['m10']/M['m00'])
    cy = int(M['m01']/M['m00'])

    # Compute euclidean distance transform for mask
    # mask_edt = morphology.distance_transform_edt(~mask, sampling=1)

    # test = cv2.circle(mask, (cx, cy), radius=3, color=128)
    # cv2.imshow("Test", test)
    # cv2.waitKey(1)

    # Find the euclidean distance between centroid and line
    # centroid_dist = mask_edt[cy, cx]
    centroid_dist = math.sqrt((cx - w/2)**2 + (cy - h/2)**2)
    
    # The maximum centroid dist is given by half of the diagonal length of the image
    diag = math.sqrt(h**2 + w**2)/2

    # Normalized fuzzy input
    cdist_fuzzy = centroid_dist/diag

    return cdist_fuzzy

def get_line_end(mask):
    h, w = mask.shape

    # Find the coordinates for the end of the line
    count = 0
    line_end = []
    indices = border_indices(h, w, flags=BORDER_LEFT + BORDER_UP + BORDER_RIGHT)
    try:
        while True:
            i, j = next(indices)
            if not line_end and mask[i, j] > 0:
                line_end.append(count)
            elif line_end and mask[i, j] == 0:
                line_end.append(count)
                break
            count += 1
        end_fuzzy = np.mean(line_end)/(2*h + w)
    except StopIteration as e:
        # Assume the line finishes at the center
        end_fuzzy = 0.5

    return end_fuzzy

def fig_to_img(fig):
    fig.canvas.draw()
    buf = fig.canvas.tostring_rgb()
    ncols, nrows = fig.canvas.get_width_height()
    img = np.fromstring(buf, dtype=np.uint8).reshape(nrows, ncols, 3)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    return img
