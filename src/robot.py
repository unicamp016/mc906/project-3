import os
import sys, time

import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

import vrep
import utils

class Drone():
	def __init__(self, config, output_dir, build_grid=False):
		self.config = config
		self.output_dir = output_dir
		self.save_path = \
			lambda name: os.path.join(
				self.output_dir,
				f"{self.config['membership_fn']}-"
				f"{self.config['defuzz_fn']}-"
				f"{self.config['inference']}-"
				f"{name}.png"
			)

		self.map_size = (10, 10) 		# Simulation map dimensions in meters
		self.map_granularity = 100 		# Pixels per meter
		self.SERVER_IP = "127.0.0.1"
		self.SERVER_PORT = 19997
		self.clientID = self.start_sim()
		self.target_handle, self.vision_handle = self.start_sensors()
		self.drone_handle = self.start_drone()

		grid_shape = tuple(map(lambda d: self.map_granularity*d, self.map_size))

		self._drone_path = ([], [])
		self._path_points = None
		self._grid_points = None
		self._path = self.init_plot()
		if build_grid:
			self._grid, self._ground_truth = self.build_grid()
		else:
			self._grid = self.init_plot()
			self._ground_truth = self.init_plot()

	def start_sim(self):
		"""
			Function to start the simulation. The scene must be running before running this code.
		    Returns:
		        clientID: This ID is used to start the objects on the scene.
		"""
		vrep.simxFinish(-1)
		clientID = vrep.simxStart(self.SERVER_IP, self.SERVER_PORT, True, True, 2000, 5)
		vrep.simxStartSimulation(clientID, vrep.simx_opmode_oneshot)
		if clientID != -1:
			utils.cprint("Connected to remoteApi server.", "g")
		else:
			vrep.simxFinish(clientID)
			sys.exit("\033[38;5;1m ERROR: Unable to connect to remoteApi server. Consider running scene before executing script.")

		return clientID

	def stop_sim(self):
		return vrep.simxStopSimulation(self.clientID, vrep.simx_opmode_oneshot)

	def get_connection_status(self):
		"""
			Function to inform if the connection with the server is active.
			Returns:
				connectionId: -1 if the client is not connected to the server.
				Different connection IDs indicate temporary disconections in-between.
		"""
		return vrep.simxGetConnectionId(self.clientID)

	def start_sensors(self):
		"""
			Function to start the sensors.
		    Returns:
		        target_handle: Target used to control the drone's movement.
				vision_handle: Contains the vision sensor handle ID.
		"""
		# Getting target handle
		res, target_handle = vrep.simxGetObjectHandle(self.clientID, "Quadricopter_target", vrep.simx_opmode_oneshot_wait)
		if(res != vrep.simx_return_ok):
			utils.cprint("Target sensor not connected.", "r")
		else:
			utils.cprint("Target sensor connected.", "g")

		# Starting vision sensor
		res, vision_handle = vrep.simxGetObjectHandle(self.clientID, "Vision_sensor", vrep.simx_opmode_oneshot_wait)
		if(res != vrep.simx_return_ok):
			utils.cprint("Vision sensor not connected.", "r")
		else:
			utils.cprint("Vision sensor connected.", "g")

		# Stream raw video feed
		vrep.simxGetVisionSensorImage(self.clientID, vision_handle, 0, vrep.simx_opmode_streaming)

		# Stream orientation and position for target handle
		vrep.simxGetObjectPosition(self.clientID, target_handle, -1, vrep.simx_opmode_streaming)
		vrep.simxGetObjectOrientation(self.clientID, target_handle, -1, vrep.simx_opmode_streaming)

		return target_handle, vision_handle

	def start_drone(self):
		"""
			Function to start the robot.
			Returns:
				robot_handle: Contains the robot handle ID.
		"""
		res, drone_handle = vrep.simxGetObjectHandle(self.clientID, "Quadricopter", vrep.simx_opmode_oneshot_wait)
		if(res != vrep.simx_return_ok):
			utils.cprint("Drone not connected.", "r")
		else:
			utils.cprint("Drone connected.", "g")

		vrep.simxGetObjectPosition(self.clientID, drone_handle, -1, vrep.simx_opmode_streaming)
		vrep.simxGetObjectOrientation(self.clientID, drone_handle, -1, vrep.simx_opmode_streaming)

		return drone_handle

	def build_grid(self):
		paths = []

		map_img_size = tuple(map(lambda d: self.map_granularity*d, self.map_size))

		# Get all path handles
		i = 0
		while True:

			utils.cprint(f"\rRetrieving path handle {i}", color="b", end="")

			res, path_handle = vrep.simxGetObjectHandle(
				clientID=self.clientID,
				objectName=f"Path{i}",
				operationMode=vrep.simx_opmode_blocking
			)

			if res != vrep.simx_return_ok:
				break

			path_dict = {
				'ori': vrep.simxGetObjectOrientation(self.clientID, path_handle, -1, vrep.simx_opmode_blocking)[1][2],
				'pos': vrep.simxGetObjectPosition(self.clientID, path_handle, -1, vrep.simx_opmode_blocking)[1][:2],
				'min_x': vrep.simxGetObjectFloatParameter(self.clientID, path_handle, vrep.sim_objfloatparam_objbbox_min_x, vrep.simx_opmode_blocking)[1],
				'min_y': vrep.simxGetObjectFloatParameter(self.clientID, path_handle, vrep.sim_objfloatparam_objbbox_min_y, vrep.simx_opmode_blocking)[1],
				'max_x': vrep.simxGetObjectFloatParameter(self.clientID, path_handle, vrep.sim_objfloatparam_objbbox_max_x, vrep.simx_opmode_blocking)[1],
				'max_y': vrep.simxGetObjectFloatParameter(self.clientID, path_handle, vrep.sim_objfloatparam_objbbox_max_y, vrep.simx_opmode_blocking)[1]
			}

			paths.append(path_dict)
			i += 1

		utils.cprint("\rFinished retrieving paths", "g")

		# Draw paths onto grid
		grid_fig, grid_ax = self.init_plot()
		gt_fig, gt_ax = self.init_plot()

		for i, p in enumerate(paths):
			ox, oy = p['pos'][0], p['pos'][1]
			img_ox, img_oy = self.img_coord(x=ox, y=oy)

			min_x = self.img_coord(x=ox + p['min_x'])
			max_x = self.img_coord(x=ox + p['max_x'])
			mid_x = (min_x + max_x)//2

			min_y = self.img_coord(y=oy + p['min_y'])
			max_y = self.img_coord(y=oy + p['max_y'])

			angle = p['ori']

			if i == 0:
				pt1 = utils.rotate((img_ox, img_oy), (min_x, min_y), angle)
				pt2 = utils.rotate((img_ox, img_oy), (max_x, max_y), angle)
				width = pt2[0] - pt1[0]
				height = pt2[1] - pt1[1]
				rect = patches.Rectangle(pt1, width, height, color=(0, 0.8, 0, 0.7), fill=True)
				grid_ax.add_patch(rect)
			else:
				pt1 = utils.rotate((img_ox, img_oy), (mid_x, min_y), angle)
				pt2 = utils.rotate((img_ox, img_oy), (mid_x, max_y), angle)
				xs = [pt1[0], pt2[0]]
				ys = [pt1[1], pt2[1]]
				gt_ax.plot(xs, ys, color=(1.0, 0, 0, 0.5))
				grid_ax.plot(xs, ys, color=(1.0, 0, 0, 0.5))

		return (grid_fig, grid_ax), (gt_fig, gt_ax)

	def read_vision_sensor(self):
		"""
			Reads the image raw data from vrep vision sensor.
			Returns:
				image: A BGR numpy array with the raw image from the sensor.
		"""

		while True:
			res, resolution, image = vrep.simxGetVisionSensorImage(
				clientID=self.clientID,
				sensorHandle=self.vision_handle,
				options=0,
				operationMode=vrep.simx_opmode_buffer
			)
			if res == vrep.simx_return_ok:
				break
		image = utils.vrep_to_array(image, resolution)
		return image

	def move_forward(self, step):
		print(f"Move forward {step}")
		return vrep.simxSetObjectPosition(
			clientID=self.clientID,
			objectHandle=self.target_handle,
			relativeToObjectHandle=self.target_handle,
			position=(step, 0, 0),
			operationMode=vrep.simx_opmode_oneshot
		)

	def steer_angle(self, angle):
		# Scale angle to work with simulation and associate left steer with
		# positive angle and right steer to negative.
		angle *= -0.1

		print(f"Steer angle {angle}")
		
		return vrep.simxSetObjectOrientation(
			clientID=self.clientID,
			objectHandle=self.target_handle,
			relativeToObjectHandle=self.target_handle,
			eulerAngles=(0, 0, angle),
			operationMode=vrep.simx_opmode_oneshot
		)

	def get_current_position(self):
		"""
			Gives the current robot position on the environment.
			Returns:
				position: Array with the (x,y,z) coordinates.
		"""
		while True:
			res, position = vrep.simxGetObjectPosition(
				clientID=self.clientID,
				objectHandle=self.drone_handle,
				relativeToObjectHandle=-1,
				operationMode=vrep.simx_opmode_buffer
			)
			if res == vrep.simx_return_ok:
				break
		return position

	def get_current_orientation(self):
		"""
			Gives the drone's current absolute orientation.
			Returns:
				orientation: Array with the euler angles (alpha, beta and gamma).
		"""

		while True:
			res, orientation = vrep.simxGetObjectOrientation(
				clientID=self.clientID,
				objectHandle=self.drone_handle,
				relativeToObjectHandle=-1,
				operationMode=vrep.simx_opmode_buffer
			)
			if res == vrep.simx_return_ok:
				break

		return orientation

	def update_drone_path(self):
		cur_pos = self.get_current_position()
		cur_img_pos = self.img_coord(x=cur_pos[0], y=cur_pos[1])
		self._drone_path[0].append(cur_img_pos[0])
		self._drone_path[1].append(cur_img_pos[1])

	def img_coord(self, x=None, y=None):

		if x is None and y is None:
			raise InvalidArgumentException

		if x is not None and y is not None:
			x = int((x+self.map_size[0]//2)*self.map_granularity)
			y = int((y+self.map_size[1]//2)*self.map_granularity)
			return x, y
		elif x is not None:
			return int((x+self.map_size[0]//2)*self.map_granularity)
		else:
			return int((y+self.map_size[1]//2)*self.map_granularity)

	def init_plot(self):
		fig, ax = plt.subplots(figsize=self.map_size, dpi=self.map_granularity)
		ax.set_xlim([0, self.map_size[0]*self.map_granularity])
		ax.set_ylim([0, self.map_size[1]*self.map_granularity])
		ax.get_xaxis().set_visible(False)
		ax.get_yaxis().set_visible(False)
		ax.axis('off')
		return fig, ax

	def save(self, image='grid'):
		if image == 'grid':
			fig, ax = self._grid
			if self._grid_points is not None:
				self._grid_points.remove()
			xs, ys = self._drone_path
			self._grid_points = ax.plot(xs, ys, color=(0, 0, 1.0, 0.5))[0]
		elif image == 'path':
			fig, ax = self._path
			if self._path_points is not None:
				self._path_points.remove()
			xs, ys = self._drone_path
			self._path_points = ax.plot(xs, ys, color=(0, 0, 1.0, 0.5))[0]
		elif image == 'ground_truth':
			fig, ax = self._ground_truth
		fig.savefig(self.save_path(image))

	def time(self):
		return vrep.simxGetLastCmdTime(self.clientID)

	@property
	def grid(self):
		grid_fig, grid_ax = self._grid
		if self._grid_points is not None:
			self._grid_points.remove()
		xs, ys = self._drone_path
		self._grid_points = grid_ax.plot(xs, ys, color=(0, 0, 1.0, 0.5))[0]
		return utils.fig_to_img(grid_fig)

	@property
	def path(self):
		path_fig, path_ax = self._path
		if self._path_points is not None:
			self._path_points.remove()
		xs, ys = self._drone_path
		self._path_points = path_ax.plot(xs, ys, color=(0, 0, 1.0, 0.5))[0]
		return utils.fig_to_img(path_fig)

	@property
	def ground_truth(self):
		gt_fig, git_ax = self._ground_truth
		return utils.fig_to_img(gt_fig)
