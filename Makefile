PYTHON = python3.6

# Directories
OUT_DIR = bin
SRC_DIR = src
LOG_DIR = logs
REPORT_DIR = report
POSTER_DIR = poster

LIBS=lib

POSTER = poster
REPORT = report
MAIN = main

.PHONY: run clean build write-report

submit: build
	@mkdir $(OUT_DIR)/code
	@rsync -azP --exclude=".git*" --exclude-from=.gitignore . $(OUT_DIR)/code
	@tar -cz -C $(OUT_DIR) $(POSTER).pdf $(REPORT).pdf code -f $(OUT_DIR)/projeto-3.tar.gz
	@rm -rf $(OUT_DIR)/code

build: $(OUT_DIR)/$(REPORT).pdf $(OUT_DIR)/$(POSTER).pdf

clean:
	@find . -name "*.pyc" -delete
	@rm -rf $(OUT_DIR)/* $(LOG_DIR)/*

run:
	@mkdir -p $(OUT_DIR) $(LOG_DIR)
	@PYTHONPATH=$(LIBS) $(PYTHON) $(SRC_DIR)/$(MAIN).py -o $(OUT_DIR) -l $(LOG_DIR) --build_grid

write-report: $(REPORT_DIR)/$(REPORT).tex
	latexmk -pdf -pvc -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)

$(OUT_DIR)/$(POSTER).pdf: $(POSTER_DIR)/$(POSTER).tex
	latexmk -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)

$(OUT_DIR)/$(REPORT).pdf: $(REPORT_DIR)/$(REPORT).tex
	latexmk -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)
