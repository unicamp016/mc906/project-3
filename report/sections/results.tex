\section{Resultados e Discussão}\label{sec:results}
  A seguir serão apresentados os resultado obtidos para os modelos A e B, bem como uma análise crítica para justificar o comportamento observado.

  \subsection{Modelo A}
  Para testar a melhor configuração de parâmetros para o modelo A, foram feitos os testes especificados na Seção~\ref{sec:methodology}, de onde obteve-se os resultados da Tabela~\ref{tab:metrics-model-a}. Nessa tabela, para cada parâmetro alterado está indicado em negrito o que obteve o melhor desempenho e foi propagado para os próximos testes. As trajetórias feitas pelo drone para cada configuração são dadas pela Figura~\ref{fig:model-a-paths}, na mesma ordem em que aparecem na tabela.

  \begin{table*}[!htbp]
    \centering
    \resizebox{0.8\textwidth}{!}{
      \begin{tabular}{|c|c|c|c|c|c|c|c|}
       \hline
       Parâmetro & Valor & MSD (px) & RMSD (px) & HD (px) & $D$ (px) & $T$ (s) \\\hline
       \multirow{3}{*}{Pertinência} & \textbf{Trapezoidal} & \textbf{8,153} & \textbf{14,968} & \textbf{86,023} & \textbf{124,113} & \textbf{140,101} \\\cline{2-7}
                                   & Gaussiana & 22,721 & 27,081 & 86,023 & 162,907 & 147,151 \\\cline{2-7}
                                   & Triangular & 15,375 & 20,547 & 86,023 & 142,493 & 139,551 \\\hline
      \multirow{2}{*}{Defuzzificação} & Bissecção & 8,153 & 14,968 & 86,023 & 124,113 & 140,101 \\\cline{2-7}
                                      & \textbf{Centróide} & \textbf{7,669} & \textbf{14,495} & \textbf{86,023} & \textbf{122,683} & \textbf{146,501} \\\hline
      \multirow{2}{*}{Inferência} & Mamdani & 7,669 & 14,495 & 86,023 & 122,683 & 146,501 \\\cline{2-7}
                                      & \textbf{Larsen} & \textbf{7,447} & \textbf{14,600} & \textbf{86,023} & \textbf{122,671} & \textbf{148,301} \\\hline
      \end{tabular}
    }
    \caption{Métricas de distância entre superfícies e tempo para terminar a trajetória para todas as configurações usando o Modelo A.}\label{tab:metrics-model-a}
  \end{table*}

  \subsubsection{Pertinência}
  Para comparar as funções de pertinência foram feitos testes com a trapezoidal, Gaussiana e a triangular. A princípio, esperava-se que a Gaussiana apresentasse melhores resultados, por realizar transições mais suaves entre os ângulos de rotação. Porém, como pode ser visto pela trajetória da Figura~\ref{fig:a-gauss-bis-mam}, a função acaba tendo dificuldades de fazer curvas mais agudas, demorando muito para alinhar-se à nova direção do caminho. Para a função triangular, notamos um padrão semelhante na Figura~\ref{fig:a-tri-bis-mam}, onde o drone demora para mudar de direção no zig-zag inicial do caminho. Isso é provavelmente devido à forma como a função triangular foi definida, onde o pico da função de pertinência que indica que a linha sai pela borda esquerda ou direita é apenas nas extremidades do domínio, ou seja, na prática as velocidades angulares mais altas quase nunca são atingidas. Já na função trapezoidal, os picos estão mais próximos um do outro, o que facilita a inferência de velocidades mais altas e resulta em curvas mais apertadas, como pode ser visto na trajetória da Figura~\ref{fig:a-trap-bis-mam}. Pela Tabela~\ref{tab:metrics-model-a}, os melhores resultados foram com a curva trapezoidal, que teve métricas MSD e RMSD melhores que as demais, mas demorou um cerca de 500ms a mais do que a configuração com a função triangular. Note que para todos os resultados na tabela, a distância de Hausdorff é igual para todas as configurações. Isso foi um erro ao setar a posição inicial do drone, que começa muito distante do início do caminho. A distância entre o ponto que marca o começo do caminho na simulação e a trajetória do drone acaba sendo a maior distância entre os percursos para todos os testes.

  \subsubsection{Defuzzificação}
  Foram feitos testes comparando as técnicas da bissecção e do centróide para defuzzificar as ativações de saída em um valor crisp. As duas técnicas são pouco sensíveis a outliers, já que buscam algum tipo de média na área sob a curva. Isso é importante, porque os testes feitos com variações das técnicas de máximos não eram sequer capazes de completar a trajetória. As variações abruptas na velocidade angular acabavam desnorteando o robô, que demora para ajustar-se ao alvo, fazendo com que a linha saia do seu campo visual. A técnica da bissecção encontra o ponto que separa a área sob a curva igualmente, enquanto o centróide busca uma média ponderada pela ativação de cada ponto no domínio. Pelas Figuras~\ref{fig:a-trap-bis-mam} e~\ref{fig:a-trap-cent-mam}, a diferença entre os percursos é quase imperceptível. Pode-se dizer que o método de centróide realiza curvas um pouco mais suaves do que o da bissecção, aproximando-se mais das quinas do zig-zag. Isso pode ser justificado pela sensibilidade maior do método da bisecção a variações abruptas na entrada fuzzy, enquanto a média ponderada do centróide altera a velocidade angular de forma gradual. Pela Tabela~\ref{tab:metrics-model-a}, vemos que o centróide obteve um resultado melhor por uma margem pequena nas métricas MSD e RMSD, apesar de levar cerca de 6s a mais para completar a trajetória.

  \subsubsection{Inferência}
  A diferença entre o método de inferência de Mamdani e de Larsen é apenas o operador de mínimo usado para obter as ativações da saída no Mamdani versus o operador produto no método de Larsen. Ao contrário do Mamdani, o método de Larsen não descaracteriza as formas das funções de pertinência na agregação. Isso é interessante quando a forma da função de pertinência modela uma interação desejada entre as ativações. As Figuras~\ref{fig:a-trap-cent-mam} e~\ref{fig:a-trap-cent-lars} mostram a trajetória realizada usando Mamdani e Larsen, respectivamente. De novo, a diferença é quase imperceptível entre as duas, mas podemos atribuir o desempenho melhor da função de Larsen pela Tabela~\ref{tab:metrics-model-a} ao fato que ela reduz a área das ativações cuja entrada é baixa. Por usarmos a função de pertinência trapezoidal, há uma boa parte do domínio de saída no qual uma das ativações é completamente preenchida e com o método de inferência de Larsen, as demais têm sua área fortemente reduzida. Pela Tabela~\ref{tab:metrics-model-a}, o método de Larsen obteve uma métrica MSD melhor, mas um RMSD um pouco pior, sinal que há mais outliers no percurso realizado por Larsen, mas na média ele acaba sendo mais próximo ao caminho real.

  \begin{figure*}[!ht]
    \centering
    \subfloat[]{
      \label{fig:a-trap-bis-mam}
      \quad\includegraphics[width=0.3\textwidth]{./images/model-a/trapezoidal-bisector-mamdani-grid.png}
    }
    \subfloat[]{
      \label{fig:a-gauss-bis-mam}
      \quad\includegraphics[width=0.3\textwidth]{./images/model-a/gaussian-bisector-mamdani-grid.png}
    }
    \subfloat[]{
      \label{fig:a-tri-bis-mam}
      \quad\includegraphics[width=0.3\textwidth]{./images/model-a/triangular-bisector-mamdani-grid.png}
    } \\
    \subfloat[]{
      \label{fig:a-trap-cent-mam}
      \quad\includegraphics[width=0.45\textwidth]{./images/model-a/trapezoidal-centroid-mamdani-grid.png}
    }
    \subfloat[]{
      \label{fig:a-trap-cent-lars}
      \quad\includegraphics[width=0.45\textwidth]{./images/model-a/trapezoidal-centroid-larsen-grid.png}
    }
    \caption{Trajetórias percorridas pelo drone em azul e caminho no V-REP em vermelho para todas as configurações testadas com o Modelo A. Na primeira fileira, são os resultados da variação da função de pertinência entre trapezoidal~\protect\subref{fig:a-trap-bis-mam}, Gaussiana~\protect\subref{fig:a-gauss-bis-mam} e triangular~\protect\subref{fig:a-tri-bis-mam}, usando técnica da bissecção para defuzzificar e o método de inferência de Mamdani. Obtendo-se os melhores resultados com a função trapezoidal, variamos a técnica de defuzzificação, comparando o resultado da bissecção~\protect\subref{fig:a-trap-bis-mam} com o centróide~\protect\subref{fig:a-trap-cent-mam}, onde o último forneceu resultados melhores. Finalmente, variamos o método de inferência, comparando o resultado de Mamdani~\protect\subref{fig:a-trap-cent-mam} com o de Larsen~\protect\subref{fig:a-trap-cent-lars}, sendo o segundo o que teve o  melhor desempenho.}\label{fig:model-a-paths}
  \end{figure*}

  \subsection{Modelo B}
  Para este modelo, usou-se o mesmo conjunto de regras do Modelo A para estimar a velocidade angular do drone. Um novo conjunto foi apresentado para inferir a velocidade do drone a partir da distância do centróide da linha ao centro da imagem.
  O objetivo dessa modelagem era diminuir o tempo para completar a trajetória, possivelmente prejudicando as métricas de distância entre superfícies. Os resultados das métricas é dado pela Tabela~\ref{tab:metrics-model-b} e as trajetórias realizadas nos experimentos pela Figura~\ref{fig:model-b-paths}.

  \begin{table*}[!htbp]
    \centering
    \resizebox{0.8\textwidth}{!}{
      \begin{tabular}{|c|c|c|c|c|c|c|c|}
       \hline
       Parâmetro & Valor & MSD (px) & RMSD (px) & HD (px) & $D$ (px) & $T$ (s) \\\hline
       \multirow{3}{*}{Pertinência} & \textbf{Trapezoidal} & \textbf{9,294} & \textbf{15,244} & \textbf{85,802} & \textbf{125,584} & \textbf{130,105} \\\cline{2-7}
                                   & Gaussiana & 23,258 & 27,662 & 86,023 & 164,605 & 140,403 \\\cline{2-7}
                                   & Triangular & 16,556 & 21,644 & 86,023 & 145,867 & 134,551 \\\hline
      \multirow{2}{*}{Defuzzificação} & Bissecção & 9,294 & 15,244 & 85,802 & 125,584 & 130,105 \\\cline{2-7}
                                      & \textbf{Centróide} & \textbf{7,040} & \textbf{14,011} & \textbf{85,802} & \textbf{120,865} & \textbf{132,506} \\\hline
      \multirow{2}{*}{Inferência} & \textbf{Mamdani} & \textbf{7,040} & \textbf{14,011} & \textbf{85,802} & \textbf{120,865} & \textbf{132,506} \\\cline{2-7}
                                  & Larsen & 8,026 & 14,586 & 85,802 & 123,001 & 129,792 \\\hline
      \end{tabular}
    }
    \caption{Métricas de distância entre superfícies e tempo para terminar a trajetória para todas as configurações usando o Modelo B.}\label{tab:metrics-model-b}
  \end{table*}

  \subsubsection{Pertinência}
  As variações na função de pertinência tiverem resultados semelhantes aos obtidos usando o Modelo A. Com a velocidade estimada pelo sistema fuzzy, os erros foram maiores para todas as configurações, mas em compensação, o tempo para completar a trajetória diminuiu. A escolha de uma função de pertinência inadequada para velocidade pode aumentar muito o erro entre os percursos, já que se a velocidade não diminuir apropriadamente para as curvas, o drone é incapaz de contornar a linha corretamente. Acredita-se que a mesma propriedade que separa as ativações de saída para a velocidade de rotação contribuiu para o resultado melhor obtido pela função trapezoidal no Modelo B. Pelos resultados da Tabela~\ref{tab:metrics-model-b}, optou-se por propagar a escolha da função trapezoidal para os próximos experimentos.

  \subsubsection{Defuzzificação}
  Para o Modelo B, esperava-se que o desempenho de todas as configurações apresentassem um padrão similar, onde o erro entre os percursos aumentava, mas o tempo para completá-los diminuía. Porém no caso da técnica do centróide, foi possível balancear bem os ganhos em velocidade nas retas com a redução de velocidade nas curvas, obtendo-se um desempenho superior a todas as configurações do Modelo A, com um valor de 7,040 para a distância média e 14,011 para a raiz da distância média quadrática de acordo com a Tabela~\ref{tab:metrics-model-b}. Além disso, o tempo para completar a curva foi menor que todos os tempos obtidos com o Modelo A, apesar de demorar cerca de 2s à mais que usando a técnica da bissecção.

  \subsubsection{Inferência}
  O método de inferência de Mamdani apresentou resultados melhores do que o de Larsen pela Tabela~\ref{tab:metrics-model-b}, provavelmente porque ele não penaliza tanto as ativações fracas da saída. Isso está refletido no tempo para completar a trajetória, que é menor para o Larsen. Esse método de inferência tipicamente implica em velocidades mais altas nas retas e mais lentas nas curvas, comparado com o do Mamdani. Porém, quando o drone move-se rápido demais ele também tende a acumular um erro maior na trajetória.

  \begin{figure*}[!ht]
    \centering
    \subfloat[]{
      \label{fig:b-trap-bis-mam}
      \quad\includegraphics[width=0.3\textwidth]{./images/model-b/trapezoidal-bisector-mamdani-grid.png}
    }
    \subfloat[]{
      \label{fig:b-gauss-bis-mam}
      \quad\includegraphics[width=0.3\textwidth]{./images/model-b/gaussian-bisector-mamdani-grid.png}
    }
    \subfloat[]{
      \label{fig:b-tri-bis-mam}
      \quad\includegraphics[width=0.3\textwidth]{./images/model-b/triangular-bisector-mamdani-grid.png}
    } \\
    \subfloat[]{
      \label{fig:b-trap-cent-mam}
      \quad\includegraphics[width=0.45\textwidth]{./images/model-b/trapezoidal-centroid-mamdani-grid.png}
    }
    \subfloat[]{
      \label{fig:b-trap-cent-lars}
      \quad\includegraphics[width=0.45\textwidth]{./images/model-b/trapezoidal-centroid-larsen-grid.png}
    }
    \caption{Trajetórias percorridas pelo drone em azul e caminho no V-REP em vermelho para todas as configurações testadas com o Modelo B. Na primeira fileira, são os resultados da variação da função de pertinência entre trapezoidal~\protect\subref{fig:b-trap-bis-mam}, Gaussiana~\protect\subref{fig:b-gauss-bis-mam} e triangular~\protect\subref{fig:b-tri-bis-mam}, usando técnica da bissecção para defuzzificar e o método de inferência de Mamdani. Obtendo-se os melhores resultados com a função trapezoidal, variamos a técnica de defuzzificação, comparando o resultado da bissecção~\protect\subref{fig:b-trap-bis-mam} com o centróide~\protect\subref{fig:b-trap-cent-mam}, onde o último forneceu resultados melhores. Finalmente, variamos o método de inferência, comparando o resultado de Mamdani~\protect\subref{fig:b-trap-cent-mam} com o de Larsen~\protect\subref{fig:b-trap-cent-lars}, sendo o primeiro o que teve o  melhor desempenho.}\label{fig:model-b-paths}
  \end{figure*}
