\section{Modelagem do Problema}\label{sec:model}
  Antes de detalhar o sistema de regras fuzzy, é importante destacar como o ambiente simulado do V-REP diverge de um ambiente real, como o cenário usado na competição do LARC 2018~\cite{nascimento2018larc}. Primeiro, o mapa da Figura~\ref{fig:vrep-scene-smooth} é definido por uma região de $10\mathrm{m} \times 10\mathrm{m}$ na cena e a trajetória é construída dentro dessa região, onde as componentes consistem de paralelepípedos com altura desprezível, largura de 5cm e comprimento varíavel. A plataforma de destino tem dimensões de $1\mathrm{m} \times 1\mathrm{m}$ e uma cor distinta da trajetória. No ambiente simulado, é muito simples segmentar a trajetória do resto da imagem a partir de um limiar superior e inferior no espaço HSV, já que a iluminação da cena é homogênea, bem como a cor das componentes da trajetória. Porém, em um ambiente real, a iluminação sob a linha introduz bastante ruído ao segmentar a imagem a partir de limiares e uma etapa de pós-processamento é necessária. Esse pós-processamento tipicamente consiste em sucessivas aplicações do operador morfológico de fechamento com um elemento estruturante $3 \times 3$ para preencher o ruído presente no interior do objeto e algumas aplicações do operador de abertura com o mesmo elemento para eliminar o ruído no fundo da imagem.

  Com relação à dinâmica do drone, a movimentação em simulação é feito através de um objeto alvo dado pela esfera verde na Figura~\ref{fig:drone}. Ao movimentar esse objeto, o drone computa a diferença em posição e orientação entre o alvo e ele mesmo e aplica um impulso proporcional à diferença para que o alvo permaneça no seu centro. Isso permite com que se aplique uma velocidade no drone movimentando apenas o alvo, sem preocupar-se com a força nos rotores que controlam as quatro hélices. Essa API é semelhante à que está disponível em muitos drones comerciais, com o Parrot AR.Drone 2.0~\cite{parrot}, que abstrai a velocidade de rotação aplicada em cada hélice e permite com que o programador mova o drone aplicando velocidades em cada eixo. Usando a API remota do V-REP, e fixando o intervalo de tempo para mandar e receber comandos do simulador em 50ms, podemos simular a API de setar a velocidade do drone através de translações de comprimento fixo a passo da simulação. Por exemplo, se movermos o alvo do drone 0,01m para frente a cada passo de simulação, ele estará se movendo a aproximadamente $\frac{0,01\mathrm{m}}{50\mathrm{ms}} = 0,2~\mathrm{m/s}$. O mesmo vale para a velocidade angular do drone.

  Para simplificar o problema, também assumimos que a altura do drone é constante e ele sempre permanece a 1,5m do chão, como especificado no guia da competição~\cite{nascimento2018larc}. Além disso, o drone já começa no ar e com orientação paralela à trajetória, de modo que a linha apareça na imagem desde o início da simulação.

  A seguir, vamos detalhar o conjunto de regras dos dois sistemas fuzzy construídos para resolver o problema, bem como as entradas e saídas de cada um.

  \subsection{Modelo A}
    Para a primeira modelagem, aplicamos uma velocidade constante de $v = \frac{0.01 \mathrm{m}}{50 \mathrm{ms}} = 0,2~\mathrm{m/s}$ para frente a todo momento. O sistema fuzzy é responsável somente por estimar a velocidade angular do drone a cada passo da simulação de forma a mantê-lo sob a linha.

    \subsubsection{Entrada}
    A única entrada para o sistema é um valor numérico normalizado entre $[0, 1]$, que nos diz o ponto onde a trajetória sai da imagem. Um frame da entrada capturada pela câmera inferior do drone é dado pela Figura~\ref{fig:raw-input}. Note como as cores do fundo e da trajetória são homogêneas, o que facilita a segmentação feita através de limiares no espaço de cor HSV. O resultado dessa limiarização aplicada à Figura~\ref{fig:raw-input} é uma imagem binária com a trajetória em primeiro plano representada pela cor branca, como pode ser visto pela Figura~\ref{fig:bin-input}.

    \begin{figure}[!htbp]
      \centering
      \includegraphics[width=0.6\textwidth]{./images/input-raw.png}
      \caption{Imagem original capturada pela câmera inferior do drone no simulador.}\label{fig:raw-input}
    \end{figure}

    \begin{figure}[!htbp]
      \centering
      \includegraphics[width=0.6\textwidth]{./images/input-yellow-mask.png}
      \caption{Imagem binária com a trajetória em primeiro plano representada pela cor branca e o fundo em preto.}\label{fig:bin-input}
    \end{figure}

    Assumindo que a linha sempre começa na borda inferior da imagem, a trajetória só pode sair da imagem pelas bordas esquerda, superior e direita. A entrada fuzzy é justamente um valor numérico que representa onde a linha sai da imagem. Assumir isso implica em um comportamento indesejado quando a trajetória tem um ângulo agudo, já que em algum momento haverá mais de um ponto na borda pelo qual a linha está saindo.

    Na Figura~\ref{fig:input-line-end}, temos uma imagem $12 \times 12$ que representa um frame da câmera inferior. Os pixels amarelos representam a linha e os vermelhos representam todos os pontos por onde a linha pode sair da imagem. No exemplo da Figura~\ref{fig:input-line-end}, temos uma borda total de comprimento $12 + 12 + 12 = 36$ pixels. O algoritmo que computa a entrada fuzzy, percorre a borda começando pelo pixel $(11, 0)$, indo até o $(0, 0)$, depois passando pelo $(0, 11)$ e finalmente pelo $(11, 11)$, onde a origem do sistema de coordenadas está no canto superior esquerdo da imagem. Contamos o número de pixels percorridos até encontrar o primeiro pixel pertencente à linha, que seria o 8\textsuperscript{o} pixel no exemplo. Continuamos percorrendo a borda até encontrar o primeiro pixel que não pertence à linha e registramos o índice do pixel anterior, que seria o 9\textsuperscript{o} no exemplo da Figura~\ref{fig:input-line-end}. Por fim, tomamos a média dos dois índices encontrados como estimativa de onde a linha sai da imagem e dividimos pelo comprimento da borda para obter o valor da entrada fuzzy. Neste caso, teríamos $\frac{8 + 9}{2 \cdot 36} \approx 0,236$. Note que esse valor é normalizado entre $[0, 1]$, já que dividimos pelo comprimento da borda, tornando-o invariante às dimensões da imagem de entrada.

    Dado esse algoritmo para computar a entrada fuzzy, é fácil perceber o motivo pelo qual ele falha para ângulos agudos. A medida que o robô está rotacionando para manter a saída da linha pela parte superior da imagem, uma nova linha aparece na borda esquerda, fazendo com que ele continue girando e fique preso, ora tentando alinhar-se a saída da trajetória e ora com a entrada.

    \begin{figure}[!htbp]
      \centering
      \includegraphics[width=0.6\textwidth]{./images/input-line-end.pdf}
      \caption{Imagem $12 \times 12$ representando um frame de entrada da câmera inferior do drone, onde os pixels amarelos representam a linha e os pixels vermelhos os pontos por onde a linha pode sair da imagem.}\label{fig:input-line-end}
    \end{figure}

    \subsubsection{Saída}
    A saída do sistema é a velocidade angular que devemos aplicar ao alvo para que o drone alinhe-se à trajetória. Como o valor absoluto da velocidade angular depende muito do modelo do drone e deve mudar significativamente para um sistema real, optou-se por computar uma saída normalizada entre $[-1, 1]$, encarregando o usuário de especificar a velocidade angular máxima desejada, que é multiplicada pela saída fuzzy antes de ser aplicada ao drone. Para a nossa simulação, o módulo da velocidade angular máxima utilizada foi $\omega = \frac{0,1 \mathrm{rad}}{50 \mathrm{ms}} = 2~\mathrm{rad/s}$, onde uma velocidade negativa corresponde a uma rotação anti-horária, e uma positiva corresponde a uma rotação no sentido horário.

    \subsubsection{Regras}
    \begin{itemize}
      \item Se a linha sai da imagem pela borda esquerda, então aplique uma velocidade angular alta para esquerda;
      \item Se a linha sai da imagem pela borda superior, então aplique uma velocidade angular baixa;
      \item Se a linha sai da imagem pela borda direita, então aplique uma velocidade angular alta para direita.
    \end{itemize}

    Os detalhes do modelo de inferência adotado, função de pertinência utilizada e método de defuzzificação serão dados na Seção~\ref{sec:methodology}.

  \subsection{Modelo B}
    Para o segundo modelo, removemos a restrição de velocidade constante e resolvemos computar a velocidade através do sistema fuzzy.

    \subsubsection{Entradas}
    O sistema possui duas entradas, a primeira é um valor entre $[0, 1]$ que nos diz onde a linha sai da imagem e a segunda é um valor entre $[0, 1]$ que representa a distância entre centróide da linha e o centro da imagem. As coordenadas $(x_c, y_c)$ do centróide de um objeto podem ser calculadas a partir da Equação~\ref{eq:centroid}, onde $m_{pq}$ é o momento geométrico de ordem $p + q$ dado pela Equação~\ref{eq:moments}, e $f(x, y)$ são os níveis de cinza de uma imagem com dimensões $M \times N$.

    \begin{equation}\label{eq:moments}
      m_{pq} = \sum_{x=0}^{M-1}\sum_{y=0}^{N-1} x^p y^q f(x, y)
    \end{equation}

    \begin{equation}\label{eq:centroid}
      x_c = \frac{m_{10}}{m_{00}}~~~\mathrm{e}~~~
      y_c = \frac{m_{01}}{m_{00}}
    \end{equation}

    Encontrada a coordenada do centróide, a entrada fuzzy é computada através da Equação~\ref{eq:cdist}. Nessa equação, dividimos a distância quadrada do centróide até o centro da imagem pela distância da origem ao centro, de forma a normalizar o valor entre $[0, 1]$ e torná-lo invariante às dimensões da imagem.

    \begin{equation}\label{eq:cdist}
      d_c = \sqrt{\frac{(x_c - M/2)^2 + (y_c - N/2)^2}{(M^2 + N^2)/4}}
    \end{equation}

    \subsubsection{Saídas}
    A saída do sistema é a velocidade angular que devemos aplicar ao alvo, bem como o módulo da velocidade com que o robô deve mover-se para frente. A saída do sistema fuzzy para velocidade é um valor no intervalo $[0, 1]$ denominado $v_{out}$, e o usuário deve especificar qual a velocidade mínima e máxima do seus sistema para obter um valor que possa ser aplicado ao robô. Para os experimentos, usamos $v_{min} = \frac{0.005 \mathrm{m}}{50 \mathrm{ms}} = 0,1~\mathrm{m/s}$ e $v_{max} = \frac{0.015 \mathrm{m}}{50 \mathrm{ms}} = 0,3~\mathrm{m/s}$. Dessa forma, temos que a velocidade final $v$ que deve ser aplicada ao alvo do drone é dada pela Equação~\ref{eq:velocity}.

    \begin{equation}\label{eq:velocity}
      v = v_{out} \cdot (v_{max} - v_{min}) + v_{min}
    \end{equation}

    \subsubsection{Regras}
    O conjunto de regras para inferir a velocidade angular é o mesmo usado para o Modelo A. A seguir temos as regras somente para inferir o módulo da velocidade linear.
    \begin{itemize}
      \item Se o centróide da linha está próximo do centro da imagem, então aplique uma velocidade alta para frente;
      \item Se o centróide da linha está a uma distância intermediária do centro da imagem, então aplique uma velocidade moderada para frente;
      \item Se o centróide da linha está a uma distância alta do centro da imagem, então aplique uma velocidade baixa para frente.
    \end{itemize}

    % Modelo be restrito, assume que a imagem sempre sai por uma das bordas e não consegue seguir linhas com um ângulo agudo
