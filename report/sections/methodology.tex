\section{Metodologia}\label{sec:methodology}
  Para verificar qual sistema e conjunto de parâmetros fornecem o melhor resultado, é necessário estabelecer métricas que permitem-nos comparar o desempenho de cada configuração. Uma das formas de comparar o percurso tomado pelo drone e a trajetória da simulação (\emph{i.e. ground truth}) é através de métricas de distância entre superfícies. Essa métricas estimam o erro entre uma superfíce $S$ e $S'$, onde $S$ e $S'$ são um conjunto de pontos na imagem que representam a trajetória do drone e a trajetória real, respectivamente. Para medir distância entre um ponto $p$ na superfície $S$ e a superfície $S'$, usamos a norma Euclideana mínima dada pela Equação~\ref{eq:min-dist}.

  \begin{equation}\label{eq:min-dist}
    d(p, S') = \min_{p' \in S'} {|| p - p' ||}_2
  \end{equation}

  Para os experimentos, optou-se por estimar o erro entre as superfícies usando a distância média entre superfícies (MSD), raiz da distância média entre as superfícies (RMSD) e a distância de Hausdorff (HD), dadas pelas Equações~\ref{eq:msd},~\ref{eq:rmsd} e~\ref{eq:hd}, respectivamente, onde $n_S$ é o número de pontos na superfície $S$. O MSD e o RMSD expressam uma média da da distância entre os caminhos, mas o RMSE é muito mais sensível a outliers, tornando-o uma métrica melhor se quisermos evitar com que haja muitos trechos no caminho onde o drone se distancia muito da linha. A distância de Hausdorff nos dá a maior distância entre duas superfícies e penaliza muito outliers, sendo uma métrica boa quando há alguma restrição forte no percurso possível que, por exemplo, descarta a corrida se o drone distanciar-se mais que determinado limiar.

  \begin{equation}\label{eq:msd}
    \mathrm{MSD} = \frac{1}{n_S + n_{S'}}(\sum_{p=1}^{n_S} d(p, S') + \sum_{p'=1}^{n_{S'}} d(p', S))
  \end{equation}

  \begin{equation}\label{eq:rmsd}
    \mathrm{RMSD} = \sqrt{\frac{1}{n_S + n_{S'}}(\sum_{p=1}^{n_S} d(p, S')^2 + \sum_{p'=1}^{n_{S'}} d(p', S)^2)}
  \end{equation}

  \begin{equation}\label{eq:hd}
    \mathrm{HD} = \max[d(S, S'), d(S', S)]
  \end{equation}

  Podemos combinar as métricas de distância em uma única métrica $D_i$ obtida a partir de uma configuração $C_i$, que queremos minimizar, dada pela Equação~\ref{eq:dist-cost}, onde priorizamos o RMSD por ser um bom meio-termo entre expressar a média e sensibilidade a outliers.

  \begin{equation}\label{eq:dist-cost}
    D = \mathrm{MSD} + 2 \cdot \mathrm{RMSD} + \mathrm{HD}
  \end{equation}

  Para determinar a melhor configuração, o único critério utilizado é a medida $D$ de erro entre as superfícies. Apesar de não ser uma métrica usada para escolher determinada configuração, também mediu-se o tempo para completar a trajetória. Nas competições o tempo tipicamente não é uma grande preocupação, já que os times têm mais do que suficiente para completar a tarefa.

  Para testar o desempenho do sistema fuzzy, para cada modelo variamos as função de pertinência, a técnica de defuzzificação e o método de inferência. A configuração inicial dos parâmetros foi a função de pertinência trapezoidal, técnica da bissecção para defuzzificação e método de inferência de Mamdani. Os valores usados para realizar os tetes de cada parâmetro são dados a seguir, na ordem em que foram alterados.

  \begin{itemize}
    \item \textbf{Pertinência}: trapezoidal, triangular e Gaussiana;
    \item \textbf{Defuzzificação}: bissecção e centróide;
    \item \textbf{Inferência}: Mamdani e Larsen.
  \end{itemize}

  Os detalhes dos parâmetros usados para definir as funções de pertinência, como a média e o desvio padrão para a função Gaussiana, estão prontamente disponíveis no código. A Figura~\ref{fig:input-mf} ilustra as diferentes funções de pertinências usadas para a entrada que nos diz onde a linha sai da imagem. Para ilustrar um passo típico do sistema, aplicamos o valor de $0,7$ nas funções de pertinência da Figura~\ref{fig:input-mf}. As ativações para cada regra do Modelo A obtidas através do método de inferência de Mamdani são dadas pela Figura~\ref{fig:steer-angle-activations}. O valor crisp obtido usando a técnica da bissecção para defuzzificar as ativações agregadas é dado pela Figura~\ref{fig:steer-angle-aggregated}.

  \begin{figure}[!ht]
    \centering
    \subfloat[]{
      \label{fig:trapezoidal-input}
      \quad\includegraphics[width=\textwidth]{./images/trapezoidal-bisector-mamdani-line-end.pdf}
    } \\
    \subfloat[]{
      \label{fig:gaussian-input}
      \quad\includegraphics[width=\textwidth]{./images/gaussian-bisector-mamdani-line-end.pdf}
    } \\
    \subfloat[]{
      \label{fig:triangular-input}
      \quad\includegraphics[width=\textwidth]{./images/triangular-bisector-mamdani-line-end.pdf}
    }
    \caption{Funções de pertinência trapezoidal~\protect\subref{fig:trapezoidal-input}, Gaussiana~\protect\subref{fig:gaussian-input} e triangular~\protect\subref{fig:triangular-input}, para o as regras relacionadas à entrada que nos diz onde a linha sai da imagem.}\label{fig:input-mf}
  \end{figure}

  \begin{figure}[!ht]
    \centering
    \subfloat[]{
      \label{fig:trapezoidal-steer}
      \quad\includegraphics[width=\textwidth]{./images/trapezoidal-bisector-mamdani-steer-angle-activations.pdf}
    } \\
    \subfloat[]{
      \label{fig:gaussian-steer}
      \quad\includegraphics[width=\textwidth]{./images/gaussian-bisector-mamdani-steer-angle-activations.pdf}
    } \\
    \subfloat[]{
      \label{fig:triangular-steer}
      \quad\includegraphics[width=\textwidth]{./images/triangular-bisector-mamdani-steer-angle-activations.pdf}
    }
    \caption{Funções de pertinência trapezoidal~\protect\subref{fig:trapezoidal-steer}, Gaussiana~\protect\subref{fig:gaussian-steer} e triangular~\protect\subref{fig:triangular-steer} pontilhadas com a ativação obtida pelo método de inferência de Mamdani de cada uma das regras relacionadas à saída da velocidade angular. Para todos os gráficos, o mesmo valor de $0,7$ para a entrada foi utilizado.}\label{fig:steer-angle-activations}
  \end{figure}

  \begin{figure}[!ht]
    \centering
    \subfloat[]{
      \label{fig:trapezoidal-aggreg}
      \quad\includegraphics[width=\textwidth]{./images/trapezoidal-bisector-mamdani-steer-angle-aggregated.pdf}
    } \\
    \subfloat[]{
      \label{fig:gaussian-aggreg}
      \quad\includegraphics[width=\textwidth]{./images/gaussian-bisector-mamdani-steer-angle-aggregated.pdf}
    } \\
    \subfloat[]{
      \label{fig:triangular-aggreg}
      \quad\includegraphics[width=\textwidth]{./images/triangular-bisector-mamdani-steer-angle-aggregated.pdf}
    }
    \caption{Funções de pertinência trapezoidal~\protect\subref{fig:trapezoidal-aggreg}, Gaussiana~\protect\subref{fig:gaussian-aggreg} e triangular~\protect\subref{fig:triangular-aggreg} pontilhadas com as ativações, obtidas através do método de inferência de Mamdani, agregadas da velocidade angular em amarelo e o valor crisp obitdo usando o método da bissecção representado por uma linha preta. Para todos os gráficos, o mesmo valor de $0,7$ para a entrada foi utilizado}\label{fig:steer-angle-aggregated}
  \end{figure}

  % Explicar variacao dos parametros
