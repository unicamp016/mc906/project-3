\documentclass[a0paper,landscape,fontscale=0.25]{baposter}

\usepackage{wrapfig}
\usepackage{lmodern}
\usepackage{enumitem}


\usepackage[utf8]{inputenc} %unicode support
\usepackage[T1]{fontenc}
\usepackage{amsmath}

\usepackage{multirow}

% Subfigures
\usepackage{subfig}
\usepackage{floatrow}

\selectcolormodel{cmyk}

\graphicspath{{figures/}} % Directory in which figures are stored


\newcommand{\compresslist}{%
\setlength{\itemsep}{0pt}%
\setlength{\parskip}{1pt}%
\setlength{\parsep}{0pt}%
}

\newenvironment{boenumerate}
  {\begin{enumerate}\renewcommand\labelenumi{\textbf\theenumi.}}
  {\end{enumerate}}



\begin{document}

\definecolor{darkgreen}{cmyk}{0.8,0,0.8,0.45}
\definecolor{lightgreen}{cmyk}{0.8,0,0.8,0.25}

\begin{poster}
{
grid=false,
columns=6,
headerborder=open, % Adds a border around the header of content boxes
colspacing=1em, % Column spacing
bgColorOne=white, % Background color for the gradient on the left side of the poster
bgColorTwo=white, % Background color for the gradient on the right side of the poster
borderColor=darkgreen, % Border color
headerColorOne=lightgreen, % Background color for the header in the content boxes (left side)
headerColorTwo=lightgreen, % Background color for the header in the content boxes (right side)
headerFontColor=white, % Text color for the header text in the content boxes
boxColorOne=white, % Background color of the content boxes
textborder=rounded, %rectangle, % Format of the border around content boxes, can be: none, bars, coils, triangles, rectangle, rounded, roundedsmall, roundedright or faded
eyecatcher=false, % Set to false for ignoring the left logo in the title and move the title left
headerheight=0.11\textheight, % Height of the header
headershape=rounded, % Specify the rounded corner in thehttps://www.overleaf.com/7532924782rpzyhjmsrpfz content box headers, can be: rectangle, small-rounded, roundedright, roundedleft or rounded
headershade=plain,
headerfont=\Large\textsf, % Large, bold and sans serif font in the headers of content boxes
%textfont={\setlength{\parindent}{1.5em}}, % Uncomment for paragraph indentation
linewidth=2pt % Width of the border lines around content boxes
}
{}
%
%----------------------------------------------------------------------------------------
%	TITLE AND AUTHOR NAME
%----------------------------------------------------------------------------------------
%
{
\vspace{5mm}
{Sistemas Fuzzy para Controlar Drones}
} % Poster title
% {\vspace{1em} Marta Stepniewska, Pawel Siedlecki\\ % Author names
% {\small \vspace{0.7em} Department of Bioinformatics, Institute of Biochemistry and Biophysics, PAS, Warsaw, Pawinskiego 5a}} % Author email addresses
{\sf\\
Rafael Figueiredo Prudencio e Giovanna Vendramini
\vspace{0.1em}\\
\small{Instituto de Computação, UNICAMP}
}
{
\includegraphics[scale=0.1]{ic-logo.png}\hspace{3mm}
\includegraphics[scale=0.01]{logo.png}
} % University/lab logo


% font size
% \large

\headerbox{Introdução}{name=introducao,column=0,span=2}{
  \begin{itemize}[noitemsep, leftmargin=*]
    \item Seguir uma linha com um drone usando apenas a câmera inferior;
    \item Utilização do simulador V-REP para implementação e prototipação;
    \item \textbf{Implementar o sistema utilizando lógica Fuzzy;}
    \item Realização de testes variando os seguintes parâmetros:
        \begin{itemize}
            \item Função de Pertinência: trapezoidal, triangular e Gaussiana;
            \item Técnica de Defuzzificação: bissecção e centróide;
            \item Método de Inferência: Mamdani e Larsen.
        \end{itemize}
    \item Minimizar a distância entre o percurso percorrido e o caminho usando métricas de distânica entre superfícies.
  \end{itemize}
}

\headerbox{Percurso}{name=imagem, column=2, span=2}{
    \textbf{Figura 1:} Percurso feito pelo modelo B com função de pertinência trapezoidal, técnica de defuzzificação do centróide e método de inferência de Mamdani.
    \begin{center}
      \includegraphics[width=0.53\textwidth]{figures/trapezoidal-centroid-mamdani-grid.png}
    \end{center}
}

\headerbox{Modelagem}{name=modelo, column=0, span=4, below=introducao}{
\begin{itemize}[noitemsep, leftmargin=*]
    \item \textbf{Modelo A}
    \begin{itemize}
        \item Velocidade linear constante com valor $v = 0,2~\mathrm{m/s}$
        \item Velocidade angular estimada usando o sistema fuzzy a partir do ponto onde a linha saida da imagem e normalizada entre $[-2, 2]$ rad/s.
    \end{itemize}
    \item \textbf{Modelo B}
    \begin{itemize}
        \item Velocidade linear estimada usando a distância entre o centróide da linha e o centro da imagem e normalizada entre $v_{min} = 0,1~\mathrm{m/s}$ e $v_{max} = 0,3~\mathrm{m/s}$
        \item Velocidade angular estimada usando o sistema fuzzy a partir do ponto onde a linha saida da imagem e normalizada entre $[-2, 2]$ rad/s.
    \end{itemize}

    As métricas usadas para medir a distância entre superfícies são dadas pelas Equações~\ref{eq:msd} a~\ref{eq:hd}.
    \begin{equation}\label{eq:msd}
        \mathrm{MSD} = \frac{1}{n_S + n_{S'}}(\sum_{p=1}^{n_S} d(p, S') + \sum_{p'=1}^{n_{S'}} d(p', S))
    \end{equation}

    \begin{equation}\label{eq:rmsd}
        \mathrm{RMSD} = \sqrt{\frac{1}{n_S + n_{S'}}(\sum_{p=1}^{n_S} d(p, S')^2 + \sum_{p'=1}^{n_{S'}} d(p', S)^2)}
    \end{equation}

    \begin{equation}\label{eq:hd}
        \mathrm{HD} = \max[d(S, S'), d(S', S)]
    \end{equation}
\end{itemize}
}

\headerbox{Resultados}{name=resultado, column=4, span=2}{
    \textbf{Tabela 1:} Métricas de distância entre superfícies e tempo para terminar a trajetória para todas as configurações usando o Modelo A.
    \begin{center}
    \centering
    \resizebox{1.0\textwidth}{!}{
      \begin{tabular}{|c|c|c|c|c|c|c|c|}
       \hline
       Parâmetro & Valor & MSD (px) & RMSD (px) & HD (px) & $D$ (px) & $T$ (s) \\\hline
       \multirow{3}{*}{Pertinência} & \textbf{Trapezoidal} & \textbf{8,153} & \textbf{14,968} & \textbf{86,023} & \textbf{124,113} & \textbf{140,101} \\\cline{2-7}
                                   & Gaussiana & 22,721 & 27,081 & 86,023 & 162,907 & 147,151 \\\cline{2-7}
                                   & Triangular & 15,375 & 20,547 & 86,023 & 142,493 & 139,551 \\\hline
      \multirow{2}{*}{Defuzzificação} & Bissecção & 8,153 & 14,968 & 86,023 & 124,113 & 140,101 \\\cline{2-7}
                                      & \textbf{Centróide} & \textbf{7,669} & \textbf{14,495} & \textbf{86,023} & \textbf{122,683} & \textbf{146,501} \\\hline
      \multirow{2}{*}{Inferência} & Mamdani & 7,669 & 14,495 & 86,023 & 122,683 & 146,501 \\\cline{2-7}
                                      & \textbf{Larsen} & \textbf{7,447} & \textbf{14,600} & \textbf{86,023} & \textbf{122,671} & \textbf{148,301} \\\hline
      \end{tabular}
    }
  \end{center}

   \textbf{Tabela 2:} Métricas de distância entre superfícies e tempo para terminar a trajetória para todas as configurações usando o Modelo B.
    \begin{center}
    \centering
    \resizebox{1.0\textwidth}{!}{
      \begin{tabular}{|c|c|c|c|c|c|c|c|}
       \hline
       Parâmetro & Valor & MSD (px) & RMSD (px) & HD (px) & $D$ (px) & $T$ (s) \\\hline
       \multirow{3}{*}{Pertinência} & \textbf{Trapezoidal} & \textbf{9,294} & \textbf{15,244} & \textbf{85,802} & \textbf{125,584} & \textbf{130,105} \\\cline{2-7}
                                   & Gaussiana & 23,258 & 27,662 & 86,023 & 164,605 & 140,403 \\\cline{2-7}
                                   & Triangular & 16,556 & 21,644 & 86,023 & 145,867 & 134,551 \\\hline
      \multirow{2}{*}{Defuzzificação} & Bissecção & 9,294 & 15,244 & 85,802 & 125,584 & 130,105 \\\cline{2-7}
                                      & \textbf{Centróide} & \textbf{7,040} & \textbf{14,011} & \textbf{85,802} & \textbf{120,865} & \textbf{132,506} \\\hline
      \multirow{2}{*}{Inferência} & \textbf{Mamdani} & \textbf{7,040} & \textbf{14,011} & \textbf{85,802} & \textbf{120,865} & \textbf{132,506} \\\cline{2-7}
                                  & Larsen & 8,026 & 14,586 & 85,802 & 123,001 & 129,792 \\\hline
      \end{tabular}
    }
  \end{center}


}

 \headerbox{Conclusão}{name=revisao,column=4,span=2,below=resultado}{
    \begin{itemize} [noitemsep, leftmargin=*]
     \item Melhor configuração para os parâmetros:
     \begin{itemize}
        \item \textbf{Modelo B};
        \item Função de Pertinência \textbf{trapezoidal};
        \item Técnica de Defuzzificação do \textbf{centróide};
        \item Método de Inferência de \textbf{Mamdani}.
     \end{itemize}
    \item Melhorias para um trabalho futuro:
    \begin{itemize}
        \item Solucionar limitação do drone em não seguir trajetórias com ângulo agudo;
        \item Analisar combinações de funções de pertinência diferentes para entrada e saída.
    \end{itemize}
    \end{itemize}
}

\end{poster}

\end{document}
